//~~~~~~~~~~~~~~~~~~~~JS~~~~~~~~~~~~~~~~~~~~
//CAC CHUC NANG
async function searchingMovies() {
	// Kiêm tra xem tìm kiếm phim theo tên phim (value = 1) hay theo tên diễn viên (value =2 )
	let s = document.getElementById('option_searching');
	let kind = s.options[s.selectedIndex].value;
	let strSearch;
	// Lấy query searching
	strSearch = $('form input').val();

	if (strSearch == '') {
		await topRatedMovies();
		return;
	}
	// Tìm kiếm phim theo tên phim
	if (kind === '1') {
		searchingMoviesByName(strSearch);
	}
	// Tìm kiếm phim theo tên diễn viên tham gia 
	else {
		searchMoviesByCast(strSearch);
	}
}

// Cac hang so cho movie API
const apiKey = 'ac97cb80f550a85ea84a010c7c2bf37f';
const baseURL = 'https://api.themoviedb.org/3/';
let configData = null;
let baseImageURL = null;

// Ham lay thong tin config cua API
async function getConfig() {
	const url = `${baseURL}configuration?api_key=${apiKey}`;
	const resp = await fetch(url);
	const result = await resp.json();
	baseImageURL = result.images.secure_base_url;
	console.log(baseImageURL);
	configData = result.images;
	console.log(configData);
}

async function evtSubmit(e) {
	e.preventDefault();
	// Dùng promise để loading xong sẽ bắt đầu xử lí tìm kiếm phim
	modalLoaing().then(searchingMovies);
}

// HÀM TẠO SPINNER
function modalLoaing() {
	$('.modal').modal('show');
	return new Promise(function(resolve, reject) {
		setTimeout(function() {
			$('.modal').modal('hide');
			resolve('hejsan');
		}, 1500);
	});
}
// ~~~~~~~~~~ HIỂN THỊ THEO TOP RATED LIST MOVIE VÀ PHÂN TRANG ~~~~~~~~~~/
let currentPageTopRated = 0;
function topRatedMovies() {
	$('form input').val('');

	$('#main').empty();

	$('#main').append(
		`<div id="top-rated-movies" class="row">
				<!-- Nơi in ra list movies -->
		</div>`
	);

	$('#main').append(`
		<nav aria-label="Page navigation example" class="w-100 mt-5" >
			<ul class="pagination justify-content-center" id="pagination_1">
				<li class="page-item disabled" id="previous-page">
					<a
						class="page-link"
						href="javascript:void(0)"
						tabindex="-1"
						aria-disabled="true"
						>Prev</a>
				</li>
				<li class="page-item " id="next-page">
					<a class="page-link" href="javascript:void(0)">Next</a>
				</li>
			</ul>
		</nav>`);

	getMoviesTopRatedPerPage(1);

	$(document).on(	'click',
		'#pagination_1 li.current-page:not(.active)',
		function() {
			return getMoviesTopRatedPerPage(+$(this).text());
		}
	);
	$('#next-page').on('click', function() {
		return getMoviesTopRatedPerPage(currentPageTopRated + 1);
	});

	$('#previous-page').on('click', function() {
		return getMoviesTopRatedPerPage(currentPageTopRated - 1);
	});
	$('#pagination_1').on('click', function() {
		$('html,body').animate({ scrollTop: 0 }, 100);
	});
}

async function getMoviesTopRatedPerPage(whichPage) {
	const reqStr = `${baseURL}movie/top_rated?api_key=${apiKey}&page=${whichPage}`;
	const response = await fetch(reqStr);
	const movies = await response.json();

	currentPageTopRated = whichPage;

	showTopRatedMovies(movies);
}

// In danh sach top rated movies theo trang
async function showTopRatedMovies(movies) {
	// In list top rated movies theo trang whichPage
	$('#top-rated-movies').empty();
	let lengthMovie, imgUrl;
	for (const m of movies.results) {
		//lengthMovie = await getLengthOfMovie(m.id);
		if (m === undefined)
			imgUrl = `https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw5WrlkUQT9BMeXPL4NVAwuD1n9hqczs__ff8Uv8XJnPfau98e&s`;
		else {
			if (m.poster_path === null) {
				imgUrl = `https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw5WrlkUQT9BMeXPL4NVAwuD1n9hqczs__ff8Uv8XJnPfau98e&s`;
			} else {
				imgUrl = `${baseImageURL}w500/${m.poster_path}`;
			}
		}
		$('#top-rated-movies').append(
			`<div class="col-md-4 py-1 mt-2">
				<div class="card shadow h-100" onclick="movieDetails(${m.id})">
					<div class="about-picture">
						<img
							class="card-img-top"
							src="${imgUrl}"
							alt="Card image cap"
						/>
					</div>
					<div class="card-body ">
						<h5 class="card-title "><a class="text-dark" href="#">${m.title}</a></h5>
						<div class="row">
						<div class="col-7">
						<p class="text-secondary pb-0 mb-0">   ${m.vote_average} <span class="fa fa-star checked"></span></p></div>
						<div class="col-5">
						<p class="text-secondary pb-0 mb-0 pl-3">
						${m.release_date} </p>
						</div>
					</div>
					</div>	
				</div>
			</div>`
		);
	}

	// Xử lí cho pagination
	// Replace the navigation items (not prev/next):
	$('#pagination_1 li')
		.slice(1, -1)
		.remove();
	getPageList(
		movies.total_pages,
		currentPageTopRated,
		movies.results.length
	).forEach(item => {
		$('<li>')
			.addClass('page-item')
			.addClass(item ? 'current-page' : 'disabled')
			.toggleClass('active', item === currentPageTopRated)
			.append(
				$('<a>')
					.addClass('page-link')
					.attr({
						href: 'javascript:void(0)'
					})
					.text(item || '...')
			)
			.insertBefore('#next-page');
	});
	// Disable prev/next when at first/last page:
	$('#previous-page').toggleClass('disabled', currentPageTopRated === 1);
	$('#next-page').toggleClass(
		'disabled',
		currentPageTopRated === movies.total_pages
	);
}
// ~~~~~~~~~~ KẾT THÚC  ~~~~~~~~~~//

// ~~~~~~~~~~ CHỨC NĂNG TÌM KIẾM MOVIE THEO TÊN MOVIE VÀ PHÂN TRANG ~~~~~~~~~~//
async function searchingMoviesByName(strSearch) {
	$('#main').empty();
	$('#main').append(`
	<div id="movies-searching-by-name" class="row">
		<!-- Nơi in ra list movies searching by name-->
	</div>
	<nav aria-label="Page navigation example" class="w-100 mt-5" >
		<ul class="pagination justify-content-center" id="pagination_4">
			<li class="page-item disabled" id="previous-page-4">
				<a
					class="page-link"
					href="javascript:void(0)"
					tabindex="-1"
					aria-disabled="true"
					>Prev</a
				>
			</li>
			<li class="page-item " id="next-page-4">
				<a class="page-link" href="javascript:void(0)">Next</a>
			</li>
		</ul>
	</nav>`);
	fillMovies(strSearch, 1);
	$(document).on(
		'click',
		'#pagination_4 li.current-page:not(.active)',
		function() {
			return fillMovies(strSearch, +$(this).text());
		});
	$('#next-page-4').on('click', function() {
		return fillMovies(strSearch, currentPageTopRated + 1);
	});
	$('#previous-page-4').on('click', function() {
		return fillMovies(strSearch, currentPageTopRated - 1);
	});
	$('#pagination_4').on('click', function() {
		$('html,body').animate({ scrollTop: 0 }, 400);
	});
}

async function fillMovies(strSearch, whichPage) {
	const reqStr = `${baseURL}search/movie/?api_key=${apiKey}&query=${strSearch}&page=${whichPage}`;
	const response = await fetch(reqStr);
	const movies = await response.json();

	if (movies.total_results === 0) {
		$('#main').empty();
		$('#main').append(`
		<div class="px-3">
			<h5>Cant find: "${strSearch}"</h5>
		</div>`);
		return true;
	}
	currentPage = whichPage;
	showMoviesSearchingByName(movies);
}

async function showMoviesSearchingByName(movies) {
	$('#movies-searching-by-name').empty();
	let lengthMovie, imgUrl;
	for (const m of movies.results) {
		// lengthMovie = await getLengthOfMovie(m.id);
		if (m === undefined)
			imgUrl = `https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw5WrlkUQT9BMeXPL4NVAwuD1n9hqczs__ff8Uv8XJnPfau98e&s`;
		else {
			if (m.poster_path === null) {
				imgUrl = `https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw5WrlkUQT9BMeXPL4NVAwuD1n9hqczs__ff8Uv8XJnPfau98e&s`;
			} else {
				imgUrl = `${baseImageURL}w500/${m.poster_path}`;
			}
		}
		$('#movies-searching-by-name').append(
			`<div class="col-md-4 py-1 mt-2">
				<div class="card shadow h-100" onclick="movieDetails(${m.id})">
					<div class="about-picture">
						<img
							class="card-img-top"
							src="${imgUrl}"
							alt="Card image cap"/>
					</div>
					
					<div class="card-body ">
						<h5 class="card-title "><a class="text-dark" href="#">${m.title}</a></h5>
						<div class="row">
						<div class="col-7">
						<p class="text-secondary pb-0 mb-0"> <span class="fa fa-star checked"></span> ${m.vote_average}</p>
						</div>
						<div class="col-5">
						<p class="text-secondary pb-0 mb-0 pl-3">
						<i class="fa fa-calendar pr-2" aria-hidden="true"></i>
						${m.release_date} </p></div>
					</div>
						
				</div>
			</div>`
		);
	}
	// Xử lí cho phần pagination
	// Replace the navigation items (not prev/next):
	$('#pagination_4 li')
		.slice(1, -1)
		.remove();
	getPageList(movies.total_pages, currentPage, movies.results.length).forEach(
		item => {
			$('<li>')
				.addClass('page-item')
				.addClass(item ? 'current-page' : 'disabled')
				.toggleClass('active', item === currentPage)
				.append(
					$('<a>')
						.addClass('page-link')
						.attr({
							href: 'javascript:void(0)'
						})
						.text(item || '...')
				)
				.insertBefore('#next-page-4');
		}
	);
	// Disable prev/next when at first/last page:
	$('#previous-page-4').toggleClass('disabled', currentPage === 1);
	$('#next-page-4').toggleClass(
		'disabled',
		currentPage === movies.total_pages
	);
}

// ~~~~~~~~~~KẾT THÚC~~~~~~~~~~//

// ~~~~~~~~~~ CHỨC NĂNG TÌM KIẾM MOVIE THEO DIỄN VIÊN VÀ PHÂN TRANG ~~~~~~~~~~//
let castsId = []; 
let arrMoviesId = []; 
let setMoviesId = null; 
let flag = 0;

function searchMoviesByCast(strSearch) {
	getCastsIdBySearching(strSearch);
}

async function getCastsIdBySearching(strSearch) {
	let reqStr = `${baseURL}search/person?api_key=${apiKey}&query=${strSearch}`;
	let response = await fetch(reqStr);
	let rs = await response.json();

	if (rs.total_results === 0) {
		$('#main').empty();
		$('#main').append(`
		<div class="px-3">
			<h5>Không tìm thấy kết quả cho : "${strSearch}"</h5>
		</div>`);
		return true;
	}
	let id_popularity_cast = 0;
	id_popularity_cast = rs.results.find(function(item) {
		return item.known_for_department == 'Acting';
	}).id;
	getMoviesBySearchingCast(id_popularity_cast);
}

async function getMoviesBySearchingCast(id) {
	// Request để lấy movies của diễn viên theo id diễn viên
	const reqStr = `${baseURL}person/${id}/movie_credits?api_key=${apiKey}`;
	const response = await fetch(reqStr);
	const rs = await response.json();
	//  Gán array movies của diễn viên vào biến toàn cục moviesOfCast để dùng cho pagination
	moviesOfCast.splice(0, moviesOfCast.length);
	moviesOfCast = rs.cast;

	$('#main').empty();
	$('#main').append(`
		<div id="movies-searching-by-cast" class="row">
			<!-- Nơi in ra list movies searching by cast-->
		</div>
		<nav aria-label="Page navigation example" class="w-100 mt-4">
			<ul class="pagination justify-content-center" id="pagination_5">
				<li class="page-item disabled" id="previous-page-5">
					<a
						class="page-link"
						href="javascript:void(0)"
						tabindex="-1"
						aria-disabled="true"
						>Prev</a
					>
				</li>
				<li class="page-item " id="next-page-5">
					<a class="page-link" href="javascript:void(0)">Next</a>
				</li>
			</ul>
		</nav>`);

	// Lấy số phim của diễn viên để phân trang
	let totalMovies = rs.cast.length;
	// console.log(totalMovies);

	// Từ đó suy ra số trang
	totalPages = Math.ceil(totalMovies / movies_per_page);
	console.log(totalPages);

	// Hiên thị movies trong trang 1 của phân trang
	showMoviesBySearchingCast(1);

	// Use event delegation, as these items are recreated later

}

async function showMoviesBySearchingCast(whichPage) {
	if (whichPage < 1 || whichPage > totalPages) return false;
	currentPage = whichPage;
	let imgUrl;
	let genresAMovie = [];
	let lengthMovie;
	$('#movies-searching-by-cast').empty();
	// Xử lí show các movies theo whichpage (là trang của phân trang)

	for (let index = 0; index < 6; index++) {
		if (index + 6 * (whichPage - 1) === moviesOfCast.length) break;

		let m = moviesOfCast[index + movies_per_page * (whichPage - 1)];

		// lengthMovie = await getLengthOfMovie(m.id);

		if (m === undefined)
			imgUrl = `https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw5WrlkUQT9BMeXPL4NVAwuD1n9hqczs__ff8Uv8XJnPfau98e&s`;
		else {
			if (m.poster_path === null) {
				imgUrl = `https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw5WrlkUQT9BMeXPL4NVAwuD1n9hqczs__ff8Uv8XJnPfau98e&s`;
			} else {
				imgUrl = `${baseImageURL}w500/${m.poster_path}`;
			}
		}
		// genresAMovie = await getGenresOfMovie(m.id);
		$('#movies-searching-by-cast').append(`
		<div class="col-md-4 py-1 mt-2">
			<div class="card shadow h-100" onclick="movieDetails(${m.id})">
				<div class="about-picture">
					<img
						class="card-img-top"
						src="${imgUrl}"
						alt="Card image cap"
					/>
				</div>
				
				<div class="card-body ">
					<h5 class="card-title "><a class="text-dark" href="#">${m.title}</a></h5>
					<div class="row">
						<div class="col-7">
						<p class="text-secondary pb-0 mb-0"> <span class="fa fa-star checked"></span> ${m.vote_average}</p></div>
						<div class="col-5">
						<p class="text-secondary pb-0 mb-0 pl-3">
						<i class="fa fa-calendar pr-2" aria-hidden="true"></i>
						${m.release_date} </p></div>
					</div>		
				</div>	
			</div>
		</div>`);
	}

	$('#pagination_5 li')
		.slice(1, -1)
		.remove();
	var arr = getPageList(totalPages, currentPage, 7);

	arr.forEach(item => {
		$('<li>')
			.addClass('page-item')
			.addClass(item ? 'current-page' : 'disabled')
			.toggleClass('active', item === currentPage)
			.append(
				$('<a>')
					.addClass('page-link')
					.attr({
						href: 'javascript:void(0)'
					})
					.text(item || '...')
			)
			.insertBefore('#next-page-5');
	});
	// Disable prev/next when at first/last page:
	$('#previous-page-5').toggleClass('disabled', currentPage === 1);
	$('#next-page-5').toggleClass('disabled', currentPage === totalPages);

	return true;
}

async function getLengthOfMovie(id) {
	const reqStr = `${baseURL}movie/${id}?api_key=${apiKey}`;
	const response = await fetch(reqStr);
	const rs = await response.json();
	return rs.runtime;
}


