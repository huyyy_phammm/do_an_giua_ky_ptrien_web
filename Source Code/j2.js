// ~~~~~~~~~~ PHẦN CHI TIẾT MOVIES VÀ PHÂN TRANG DANH SÁCH DIỄN VIÊN CỦA PHIM ~~~~~~~~~~//
let listCastsOfMovie = [];
let currentListCastsMoviePage = 0;
let totalCastsOfMovie = 0;
let totalCastPage = 0;

async function movieDetails(id) {
	modalLoaing();
	const reqStr = `${baseURL}movie/${id}?api_key=${apiKey}`;
	const response = await fetch(reqStr);
	const rs = await response.json();
	// Lấy thể loại phim
	let genresMovie = await getGenresOfMovie(id);

	// Công ty sx phim
	let productionCompanies = [];
	rs.production_companies.forEach(entry => {
		productionCompanies.push(entry.name);
	});

	// Quốc gia sx phim
	let productionCountries = [];
	rs.production_countries.forEach(entry => {
		productionCountries.push(entry.name);
	});

	// Kiểm tra ảnh poster có null không
	let img = ``;
	if (rs.poster_path == null) {
		img = `https://via.placeholder.com/300/09f/fff.png`;
	} else {
		img = `${baseImageURL}w500/${rs.poster_path}`;
	}

	// Lấy đạo diễn phim
	let directors = [];
	directors = await getDirectors(id);

	// Lấy reviews phim
	const reviews = await getReviewsMovie(id);
	// console.log('Director: ' + directors.join(', '));
	const castsOfMovie = await getCastsOfAMovie(id);
	$('#main').empty();

	$('html,body').animate({ scrollTop: 0 }, 500);
	$('#main').append(`
    <div class="row py-2 ml-5 mt-4">
				<div class="col-sm-4">
					<div class="card shadow h-100">
						<div class="about-picture">
							<img
								class="card-img-top"
								src="${img}"
								alt="Card image cap"
							/>
						</div>	
					</div>
				</div>
				<div class="col-sm-6 text-left offset-top-30 ml-5">
					<h2>${rs.original_title}</h2>
					<div class="review-2 row">
						<div class="col-6">		
						</div>
					</div>
					<br />
					<p>
						${rs.overview}
					</p>

					<br />
					<dl class="row">
						<dt class="col-sm-3">Đạo diễn:</dt>
						<dd class="col-sm-9">
							${directors.join(', ')}
						</dd>
						<dt class="col-sm-3">Thể loại:</dt>
						<dd class="col-sm-9">
							${genresMovie.join(', ')}
						</dd>

						<dt class="col-sm-3">Ngày phát hành:</dt>
						<dd class="col-sm-9">
							${rs.release_date}
						</dd>

						<dt class="col-sm-3">Thời lượng:</dt>
						<dd class="col-sm-9">
							${rs.runtime} min
						</dd>

						<dt class="col-sm-3 text-truncate">
							Quốc gia:
						</dt>
						<dd class="col-sm-9">
							${productionCountries.join(', ')}
						</dd>

						<dt class="col-sm-3">Nhà sản xuất:</dt>
						<dd class="col-sm-9">
							<dl class="row px-3">
								${productionCompanies.join(', ')}
							</dl>
						</dd>
                        <dt class="col-sm-6"></dt>      
                    </dl>
                    <div>
                        <i class="fab fa-imdb fa-3x rounded ">
                            <span class="imdbscore"> : ${
								rs.vote_average
							}&nbsp</span>
                        </i>
                    </div>
				</div>
			</div>
            
			<div class="container mt-5 ml-4">
                <h2>Diễn viên: </h2>
                <hr />
				<div class="row py-2" id="casts-of-movie">
					<!-- Nơi show các diễn viên của phim -->
				</div>
				<nav aria-label="Page navigation example" class="w-100">
					<ul class="pagination justify-content-center" id="pagination_3">
						<li class="page-item disabled" id="previous-page-3">
							<a
								class="page-link"
								href="javascript:void(0)"
								tabindex="-1"
								aria-disabled="true"
								>Prev</a>
						</li>
						<li class="page-item " id="next-page-3">
							<a class="page-link" href="javascript:void(0)">Next</a>
						</li>
					</ul>
				</nav>
			${reviews}
	`);
	await getCastsOfAMovie(id);
}
async function getGenresOfMovie(id) {
	const reqStr = `${baseURL}movie/${id}?api_key=${apiKey}`;
	const response = await fetch(reqStr);
	const rs = await response.json();
	// Lấy các thể loại của phim
	let genresMovie = [];
	rs.genres.forEach(entry => {
		genresMovie.push(entry.name);
	});
	return genresMovie;
}


async function getCastsOfAMovie(id) {
	// Lay thong tin ve dien vien cua phim
	const reqStrCast = `${baseURL}movie/${id}/credits?api_key=${apiKey}`;
	const responseCast = await fetch(reqStrCast);
	const rsCast = await responseCast.json();

	// Lấy mảng các diễn viên của phim
	listCastsOfMovie = rsCast.cast;
	totalCastsOfMovie = listCastsOfMovie.length;
	totalCastPage = Math.ceil(totalCastsOfMovie / 6); 
	showListCastsOfMovie(1);

	$(document).on(
		'click',
		'#pagination_3 li.current-page:not(.active)',
		function() {
			return showListCastsOfMovie(+$(this).text());
		}
	);
	$('#next-page-3').on('click', function() {
		return showListCastsOfMovie(currentListCastsMoviePage + 1);
	});

	$('#previous-page-3').on('click', function() {
		return showListCastsOfMovie(currentListCastsMoviePage - 1);
	});
}

function showListCastsOfMovie(whichPage) {
	if (whichPage < 1 || whichPage > totalCastPage) return false;
	currentListCastsMoviePage = whichPage;

	$('#casts-of-movie').empty();
	// Xử lí show các movies theo whichpage (là trang của phân trang)

	for (let index = 0; index < 6; index++) {
		if (index + 6 * (whichPage - 1) === listCastsOfMovie.length) break;

		let c = listCastsOfMovie[index + movies_per_page * (whichPage - 1)];

		let imgUrl = ``;
		if (c.profile_path === null) {
			imgUrl = `img/no avatar.jpg`;
		} else {
			imgUrl = `${baseImageURL}w92/${c.profile_path}`;
		}
		$('#casts-of-movie').append(`
		<div class="col-2">
			<div >
				<img src="${imgUrl}" alt="" />
				<div class="mt-2">
					<h6 ><strong> 
					<a href="javascript:void(0)" onclick="castDetails(${c.id})" class="text-dark btn px-0"> ${c.name}
					</a>
					</strong></h6>
				</div>
			</div>
		</div>`);
	}
}
// Hàm lấy các đạo diễn của phim
async function getDirectors(id) {
	const reqStrDir = `${baseURL}movie/${id}/credits?api_key=${apiKey}`;
	const responseDir = await fetch(reqStrDir);
	const rsDir = await responseDir.json();
	var directors = [];
	rsDir.crew.forEach(function(entry) {
		if (entry.job === 'Director') {
			directors.push(entry.name);
		}
	});
	return directors;
}

// ~~~~~~~~~~KẾT THÚC MOVIE DETAILS~~~~~~~~~~//
// ~~~~~~~~~~ PHẦN REVIEW MOVIES ~~~~~~~~~~//
async function getReviewsMovie(id) {
	const reqStr = `${baseURL}movie/${id}/reviews?api_key=${apiKey}`;
	const response = await fetch(reqStr);
	const rs = await response.json();
	let result = `
	<div class="row py-2">
	<div class="col"><hr /></div>
	<div class="col-auto"><h2>Reviews Film</h2></div>
	<div class="col"><hr /></div>`;
	for (const r of rs.results) {
		result += `
		<div class="card mb-2 w-100">
		<div class="card-body">
		<div class="row">
			<div class="col-md-2">
				<p class="text-secondary text-center"> ID: ${r.id} </p>
			</div>
			<div class="col-md-10">
				<p>
					<strong>${r.author}</strong> 
					</a>
				</p>
				<p>
					${r.content}
				</p>
			</div>
		</div>
	</div>
</div>`;}
	result += `</div>`;
	return result;
}
// ~~~~~~~~~~~~~~~~~~~~ KẾT THÚC ~~~~~~~~~~~~~~~~~~~~//
// ~~~~~~~~~~ PHẦN CAST DETAILS VÀ PHÂN TRANG DANH SÁCH MOVIES CỦA DIỄN VIÊN~~~~~~~~~~//

let moviesOfCast = []; 
let totalPages = 0; 
let page = 0; 
let maxLength = 0; 
const movies_per_page = 6; 
let currentPage = 0;

// Hàm xem thông tin chi tiết diễn viên (liên kết từ xem chi tiết movie)
async function castDetails(id) {
	modalLoaing();
	// Request để lấy thông tin diễn viên theo id
	const reqStr = `${baseURL}person/${id}?api_key=${apiKey}`;
	const response = await fetch(reqStr);
	const rs = await response.json();

	$('#main').empty();
	$('html,body').animate({ scrollTop: 0 }, 500);
	$('#main').append(`
	<div class="col-4 px-2 shadow border-top" style="background-color:#f6f9fc";>
		<div class="mt-3">
			<div class="text-center">
				<img
					class="rectangular avatar-cast shadow"
					src="${baseImageURL}w400/${rs.profile_path}"
					alt="Card image cap"
				/>
			</div>
			<div class=" mt-3">
				<blockquote class="text-center w-75 mx-auto">
					<h4>
						${rs.name}
					</h4>
					<cite title="Source Title">Diễn viên</cite>
				</blockquote>
			</div>
		</div>
		<div class="pl-2">		
			<br />
			<br />
			<dl class="row pl-2">
				<dt class="col-sm-3 pr-0">Tiểu sử:</dt>
				<dd class="col-sm-9 pl-0">
					${rs.biography}
				</dd>
				<dt class="col-sm-3 pr-0">Ngày sinh:</dt>
				<dd class="col-sm-9 pl-0">
					${rs.birthday}
				</dd>
				<dt class="col-sm-3 pr-0">Giới tính:</dt>
				<dd class="col-sm-9 pl-0">
					${rs.gender == 2 ? 'Nam' : 'Nữ'}
				</dd>
			</dl>
		</div>
	</div>`);
	await getMoviesOfCast(id);
}

// Hàm lấy các movie mà diễn viên đã tham gia theo id diễn viên
async function getMoviesOfCast(id) {
	// Request để lấy movies của diễn viên theo id diễn viên
	const reqStr = `${baseURL}person/${id}/movie_credits?api_key=${apiKey}`;
	const response = await fetch(reqStr);
	const rs = await response.json();

	//  Gán array movies của diễn viên vào biến toàn cục moviesOfCast để dùng cho pagination
	moviesOfCast = [];
	moviesOfCast = rs.cast;

	$('#main').append(`
	<div class="col-8">
		<h2 class ="ml-4"><strong>Danh sách phim đã tham gia</strong></h2>
		<hr />
		<div id="movies_of_cast">
			<!- Nơi chứa list movies of cast -->
		</div>
		<nav aria-label="Page navigation example" class="w-100">
			<ul class="pagination justify-content-center" id="pagination_2">
				<li class="page-item disabled" id="previous-page-1">
					<a
						class="page-link"
						href="javascript:void(0)"
						tabindex="-1"
						aria-disabled="true"
						>Prev</a>
				</li>
				<li class="page-item " id="next-page-1">
					<a class="page-link" href="javascript:void(0)">Next</a>
				</li>
			</ul>
		</nav>
	</div>`);
	// Lấy số phim của diễn viên để phân trang
	let totalMovies = rs.cast.length;
	// console.log(totalMovies);

	// Từ đó suy ra số trang
	totalPages = Math.ceil(totalMovies / movies_per_page);
	console.log(totalPages);

	// Hiên thị movies trong trang 1 của phân trang
	showMoviesOfCast(1);

	// Use event delegation, as these items are recreated later
	$(document).on(
		'click',
		'#pagination_2 li.current-page:not(.active)',
		function() {
			return showMoviesOfCast(+$(this).text());
		}
	);
	$('#next-page-1').on('click', function() {
		return showMoviesOfCast(currentPage + 1);
	});

	$('#previous-page-1').on('click', function() {
		return showMoviesOfCast(currentPage - 1);
	});

	$('#pagination_2').on('click', function() {
		$('html,body').animate({ scrollTop: 0 }, 500);
	});
}

// Hàm xử lí phân trang khi có nhiều page
function getPageList(totalPages, page, maxLength) {
	if (maxLength < 5) throw 'maxLength must be at least 5';

	function range(start, end) {
		return Array.from(Array(end - start + 1), (_, i) => i + start);
	}
	var sideWidth = maxLength < 9 ? 1 : 2;
	var leftWidth = (maxLength - sideWidth * 2 - 3) >> 1;
	var rightWidth = (maxLength - sideWidth * 2 - 2) >> 1;
	if (totalPages <= maxLength) {
		// no breaks in list
		return range(1, totalPages);
	}
	if (page <= maxLength - sideWidth - 1 - rightWidth) {
		// no break on left of page
		return range(1, maxLength - sideWidth - 1)
			.concat([0])
			.concat(range(totalPages - sideWidth + 1, totalPages));
	}
	if (page >= totalPages - sideWidth - 1 - rightWidth) {
		// no break on right of page
		return range(1, sideWidth)
			.concat([0])
			.concat(
				range(
					totalPages - sideWidth - 1 - rightWidth - leftWidth,
					totalPages
				)
			);
	}
	// Breaks on both sides
	return range(1, sideWidth)
		.concat([0])
		.concat(range(page - leftWidth, page + rightWidth))
		.concat([0])
		.concat(range(totalPages - sideWidth + 1, totalPages));
}
// Hàm in movies của diễn viên theo từng trang trong phân trang
async function showMoviesOfCast(whichPage) {
	if (whichPage < 1 || whichPage > totalPages) return false;
	currentPage = whichPage;
	let imgUrl;
	let genresAMovie = [];
	$('#movies_of_cast').empty();
	// Xử lí show các movies theo whichpage (là trang của phân trang)
	for (let index = 0; index < movies_per_page; index++) {
		if (index + movies_per_page * (whichPage - 1) === moviesOfCast.length)
			break;
		let m = moviesOfCast[index + movies_per_page * (whichPage - 1)];
		if (m === undefined) imgUrl = `img/img-not-found.jpg`;
		else {
			if (m.backdrop_path === null) {
				imgUrl = `img/img-not-found.jpg`;
			} else {
				imgUrl = `${baseImageURL}w300/${m.backdrop_path}`;
			}
		}
		// genresAMovie = await getGenresOfMovie(m.id);
		$('#movies_of_cast').append(`
			<div class="row py-2 mb-3 pl-3">
				<div class="col-5 mr-2 ml-2">
					<img class="rectangular" src="${imgUrl}" style="width: 200px;"/>
				</div>
				<div class="col-6 info-movie ">
					<div class="mt-2 pl-2">
						<h4>
							<strong>
								<a href="#" class="text-dark">${m.title}</a
								></strong>
						</h4>
						<span class="fa fa-star checked"></span> :
						${m.vote_average}/10
						<dl class="row mt-2 mb-0">
							<dt class="col-sm-5 pr-0">
								Ngày phát hành :
							</dt>
							<dd class="col-sm-7 px-0">
								${m.release_date}
							</dd>
							<dt class="col-sm-5 pr-0">Số lượng vote:</dt>
							<dd class="col-sm-7 px-0">
								${m.vote_count}
							</dd>
						</dl>
						<p class="font-italic text-right text-secondary">
							đóng vai ${m.character}
						</p>
					</div>
				</div>
			</div>`);
	}
	return true;
}
